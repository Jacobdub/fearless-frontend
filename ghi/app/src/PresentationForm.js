import React, { useEffect, useState } from 'react';
import {
    BrowserRouter,
    Routes,
    Route,
    useParams
  } from 'react-router-dom'

function PresentationForm (props) {

    //CHANGE INPUTS WITH USER'S ENTRIES------------------------------------------------------
    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [selectedConference, setSelectedConference] = useState('');

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        return setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        return setEmail(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        return setCompanyName(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        return setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        return setSynopsis(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        return setSelectedConference(value);
    }

    //GETS LIST OF CONFERENCES INSIDE THE DATA VARIABLE---------------------------------------------

    const [conferences, setConferences] = useState([]);

    const fetchData = async () => {

        const url = `http://localhost:8000/api/conferences/`;

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            //ADD LIST OF CONFERENCES FOR THE SELECT BUTTON-----------------------------------------
            setConferences(data.conferences)
        }
    }

    // useEffect() is a hook to make fectch requests to get data:
    useEffect(() => {
        fetchData();
    }, []);

    //SAVE FORM WHEN CLICKED ON "Ceate" BUTTON---------------------------------------------------
    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.presenter_name = name;
        data.company_name = companyName;
        data.presenter_email = email;
        data.title = title;
        data.synopsis = synopsis;



        console.log(data);

        //Sends data to the server:
        const presentationUrl = `http://localhost:8000${selectedConference}presentations/`;

        console.log(presentationUrl)

        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setName('');
            setCompanyName('');
            setEmail('');
            setTitle('');
            setSynopsis('');
            setSelectedConference('');
        }
    }

    //RETURNS THE FORM FOR A NEW PRESENTATION--------------------------------------------------------
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new presentation</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                  <label htmlFor="presenter_name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={email} onChange={handleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                  <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                  <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                  <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="synopsis">Synopsis</label>
                  <textarea value={synopsis} onChange={handleSynopsisChange} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                </div>
                <div className="mb-3">
                  <select value={selectedConference} onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                    {conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                            );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      );

}

export default PresentationForm;
